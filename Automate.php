<?php

/**
 * Class Automate 
 */
class Automate
{
	private $data;
	private $input;

	/**
	 * Constructeur
	 */
	function __construct()
	{
		for ($i=0; $i < 20 ; $i++) { 
			$this->data[] = rand (0, 100);
		}
	}

	/**
	 * Calculer la somme
	 */
	public function calculate() {

		foreach ($this->data as $key => $value) {
			for ($i = 0, $n = count($this->data); $i < $n-1; $i++) { 
				$arraySliceSum = array_sum([$value, $this->data[$i+1]]);

			  	if ($arraySliceSum == $this->input) {
			  		return true;
			  	}
			}
		}

		return false;
	}

	/**
	 * Vérifier si l'input est > à 0 et <= à 100
	 *
	 * @param int $input
	 */
	public static function verifieInput($input) {
		if ($input > 0 && $input <= 100) {
			return true;
		} else {
			return false;
		}
	}

	public function getInput() {
		return $this->input;
	}

	/**
	 * set input
	 * @param int $input
	 */
	public function setInput($input) {
		$this->input = $input;

		return $this;
	}
}

print "Entrez un nombre entier strictement supérieur à 0 et égal ou inférieur à 100 \n";

$input = trim(fgets(STDIN));
$automate = new Automate();

// Vérification de la valeur saisie
if (!$automate::verifieInput($input)) {
	$result = false;
} else {
	$automate->setInput($input);
	// Calcuer
	$result = $automate->calculate();
}

// retour true ou false
print ($result) ? "true \n" : "false \n";
