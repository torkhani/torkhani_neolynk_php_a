<?php

abstract class Product {
	private $name;
	private $reference;
}

/**
 * Class Dentifrices
 */
class Dentifrices extends Product {
	//code spécifique aux Dentifrices
}

/**
 * Class Shampooings
 */
class Shampooings extends Product {
	//code spécifique aux Shampooings
}

/**
 * Class Farines
 */
class Farines extends Product {
	//code spécifique aux Farines
}

/**
 * Class ProductFactory
 *
 * @return Product
 * @throws \Exception
 */
class ProductFactory 
{
	public static function build($productType, $sku, $name)
	{
		if(class_exists($productType))
		{
			return new $productType($sku, $name);
		}
		else {
			throw new Exception("Invalid product type given.");
		}
	}
}

/**
 * Class Catalogues
 */
class Catalogues {

	private $productFactory;

	public function __construct(ProductFactory $productFactory) {
		$this->productFactory = $productFactory;

		//...
		$this->productFactory->build('Dentifrices');
		$this->productFactory->build('Shampooings');
		$this->productFactory->build('Farines');
	}
}
